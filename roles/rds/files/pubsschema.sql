CREATE DATABASE if not exists pubs;

USE pubs;

CREATE TABLE if not exists authors
(
   au_id          varchar(11) 	    NOT NULL,
   au_lname       varchar(40)       NOT NULL,
   au_fname       varchar(20)       NOT NULL,
   phone          char(12)          NOT NULL DEFAULT 'UNKNOWN',
   address        varchar(40)           NULL,
   city           varchar(20)           NULL,
   state          char(2)               NULL,
   zip            char(5)               NULL,
   contract       bit               NOT NULL,
   PRIMARY KEY(au_id)
);

CREATE TABLE if not exists publishers
(
   pub_id         char(4)           NOT NULL,
   pub_name       varchar(40)           NULL,
   city           varchar(20)           NULL,
   state          char(2)               NULL,
   country        varchar(30)           NULL DEFAULT 'USA',
   PRIMARY KEY(pub_id)
);

CREATE TABLE if not exists titles
(
   title_id       varchar(6)	    NOT NULL,
   title          varchar(80)       NOT NULL,
   type           char(12)          NOT NULL DEFAULT 'UNDECIDED',
   pub_id         char(4)               NULL,
   price          decimal(4,2)          NULL,
   advance        decimal(10,2)          NULL,
   royalty        int                   NULL,
   ytd_sales      int                   NULL,
   notes          varchar(200)          NULL,
   PRIMARY KEY(title_id)
);


CREATE TABLE if not exists titleauthor
(
   au_id          varchar(11)	    NOT NULL,
   title_id       varchar(6)	    NOT NULL,
   au_ord         tinyint               NULL,
   royaltyper     int                   NULL,
   FOREIGN KEY(title_id) REFERENCES titles(title_id),
   PRIMARY KEY(au_id, title_id)
);

CREATE TABLE if not exists stores
(
   stor_id        char(4)           NOT NULL,
   stor_name      varchar(40)           NULL,
   stor_address   varchar(40)           NULL,
   city           varchar(20)           NULL,
   state          char(2)               NULL,
   zip            char(5)               NULL,
   PRIMARY KEY(stor_id)
);

CREATE TABLE if not exists sales
(
   stor_id        char(4)           NOT NULL,
   ord_num        varchar(20)       NOT NULL,
   ord_date       char(8)           NOT NULL,
   qty            smallint          NOT NULL,
   payterms       varchar(12)       NOT NULL,
   title_id       varchar(6)	    NOT NULL,
   FOREIGN KEY(title_id) REFERENCES titles(title_id),
   FOREIGN KEY(stor_id) REFERENCES stores(stor_id),
   PRIMARY KEY(stor_id, ord_num, title_id)
);

CREATE TABLE if not exists roysched
(
   title_id       varchar(6)	    NOT NULL,
   lorange        int                   NULL,
   hirange        int                   NULL,
   royalty        int                   NULL,
   FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE if not exists discounts
(
   discounttype   varchar(40)       NOT NULL,
   stor_id        char(4) 		NULL,
   lowqty         smallint              NULL,
   highqty        smallint              NULL,
   discount       dec(4,2)          NOT NULL,
   FOREIGN KEY(stor_id) REFERENCES stores(stor_id)
);

CREATE TABLE if not exists jobs
(
   job_id         int          NOT NULL auto_increment,
   job_desc       varchar(50)       NOT NULL DEFAULT 'New Position - title not formalized yet',
   min_lvl        int           NOT NULL,
   max_lvl        int           NOT NULL,
   PRIMARY KEY(job_id)
);


CREATE TABLE if not exists employee
(
   emp_id         char(9)     	    NOT NULL,
   fname          varchar(20)       NOT NULL,
   minit          char(1)               NULL,
   lname          varchar(30)       NOT NULL,
   job_id         int          NOT NULL DEFAULT '1',
   job_lvl        int	    		NOT NULL DEFAULT '10',
   pub_id         char(4)           NOT NULL DEFAULT '9952',
   hire_date      char(8)           NOT NULL DEFAULT '19950818',
   PRIMARY KEY(emp_id),
   FOREIGN KEY(job_id) REFERENCES jobs(job_id),
   FOREIGN KEY(pub_id) REFERENCES publishers(pub_id)
);
