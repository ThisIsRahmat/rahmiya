<?php
# Script to show data from the authors table

$servername = $_GET["dbsrv"];
$username = $_GET["dbuser"];
$password = $_GET["dbpass"];
$dbname = "pubs";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT au_id, au_fname, au_lname FROM authors";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["au_id"]. " - Name: " . $row["au_fname"]. " " . $row["au_lname"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
